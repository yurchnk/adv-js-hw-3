/* ===========================Task 1=============================== */
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"]; 
let mergedArr = [...clients1, ...clients2];
let uniqMergedArr = [...new Set(mergedArr)];
console.log('Task 1', uniqMergedArr);
/* ================================================================ */

/* ===========================Task 2=============================== */
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
];
let charactersShortInfo = [];
for (var {name: name, lastName: lastName, age: age } of characters) {
    charactersShortInfo.push({name,lastName,age}); 
}
console.log('Task 2', charactersShortInfo);
/* ================================================================ */

/* ===========================Task 3=============================== */
const user1 = {
    namee: "John",
    years: 30
};
let {namee, years, isAdmin = false} = user1;
console.log('Task 3', namee, years, isAdmin);
/* ================================================================ */

/* ===========================Task 4=============================== */
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
};
  
const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
};
  
const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
};
  
const fullProfile = {
    ...satoshi2018,
    ...satoshi2019,
    ...satoshi2020
};
  
console.log('Task 4', fullProfile);
/* ================================================================ */

/* ===========================Task 5=============================== */
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, 
{
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, 
{
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}
let newArr = [];
let [book1, book2, book3] = books;
newArr.push(book1, book2, book3, bookToAdd);
console.log('Task 5', newArr);
  /* ================================================================ */
  /* ===========================Task 6=============================== */
const employee = {
    name: "John Doe",
    position: "Software Engineer"
};

const newEmployee = {
    ...employee,
    age: 20,
    salary: 1500
};

console.log('Task 6', newEmployee);
  /* ================================================================ */
  /* ===========================Task 7=============================== */
  const array = ['value', () => 'showValue'];

let [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'
/* ================================================================ */